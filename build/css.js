const _ = require('lodash')
const gulp = require('gulp')
const postcss = require('gulp-postcss')
const cssnano = require('cssnano')
const rename = require('gulp-rename')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const util = require('gulp-util')

var defaultOptions = {
  src: null,
  dest: null,
  sourcemap: false,
  minify: false
}

module.exports = (options) => {
  options = _.assign({}, defaultOptions, options)

  let plugins = [];

  if (options.minify) {
    plugins.push(cssnano);
  }

  return gulp.src(options.src)
    .pipe(options.sourcemap ? sourcemaps.init() : util.noop())
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(rename((path) => { path.extname = ".css" }))
    .pipe(options.sourcemap ? sourcemaps.write('.') : util.noop())
    .pipe(options.dest ? gulp.dest(options.dest) : util.noop())
}