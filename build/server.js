const path = require("path")
const es = require("event-stream")
const util = require("gulp-util")
const http = require("http")
const fs = require("fs")
const connect = require("connect")
const liveReload = require("connect-livereload")
const serveIndex = require('serve-index')
const serveStatic = require('serve-static')
const tiny_lr = require("tiny-lr")

var apps = [];

var ConnectApp = (function() {
  function ConnectApp(options) {
    this.name = options.name || "Server";
    this.port = options.port || "8080";
    this.root = options.root || path.dirname(module.parent.id);
    this.host = options.host || "localhost";
    this.debug = options.debug || false;
    this.silent = options.silent || false;
    this.livereload = options.livereload || false;
    this.middleware = options.middleware || void 0;
    this.serverInit = options.serverInit || void 0;
    this.fallback = options.fallback || void 0;
    this.index = options.index;
    if (options.open) {
      this.oldMethod("open");
    }
    this.sockets = [];
    this.app = void 0;
    this.lr = void 0;
    this.run();
  }

  ConnectApp.prototype.run = function() {
    this.app = connect();
    this.handlers().forEach((function(_this) {
      return function(middleware) {
        if (typeof middleware === "object") {
          return _this.app.use(middleware[0], middleware[1]);
        } else {
          return _this.app.use(middleware);
        }
      };
    })(this));
    this.app.use(serveIndex(typeof this.root === "object" ? this.root[0] : this.root));
    this.server = http.createServer(this.app);
    if (this.serverInit) {
      this.serverInit(this.server);
    }
    return this.server.listen(this.port, (function(_this) {
      return function(err) {
        var sockets, stopServer, stoped;
        if (err) {
          return _this.log("Error on starting server: " + err);
        } else {
          _this.log(_this.name + " started http://" + _this.host + ":" + _this.port);
          stoped = false;
          sockets = [];
          _this.server.on("close", function() {
            if (!stoped) {
              stoped = true;
              return _this.log(_this.name + " stopped");
            }
          });
          _this.server.on("connection", function(socket) {
            _this.logDebug("Received incoming connection from " + (socket.address().address));
            _this.sockets.push(socket);
            return socket.on("close", function() {
              return _this.sockets.splice(_this.sockets.indexOf(socket), 1);
            });
          });
          _this.server.on("request", function(request, response) {
            return _this.logDebug("Received request " + request.method + " " + request.url);
          });
          _this.server.on("error", function(err) {
            return _this.log(err.toString());
          });
          stopServer = function() {
            if (!stoped) {
              _this.sockets.forEach(function(socket) {
                return socket.destroy();
              });
              _this.server.close();
              return process.nextTick(function() {
                return process.exit(0);
              });
            }
          };
          process.on("SIGINT", stopServer);
          process.on("exit", stopServer);
          if (_this.livereload) {
            tiny_lr.Server.prototype.error = function() {};
            _this.lr = tiny_lr();
            _this.lr.listen(_this.livereload.port);
            return _this.log("LiveReload started on port " + _this.livereload.port);
          }
        }
      };
    })(this));
  };

  ConnectApp.prototype.handlers = function() {
    var steps;
    steps = this.middleware ? this.middleware.call(this, connect, this) : [];
    if (this.livereload) {
      if (typeof this.livereload === "boolean") {
        this.livereload = {};
      }
      if (!this.livereload.port) {
        this.livereload.port = 35729;
      }
      steps.unshift(liveReload(this.livereload));
    }
    if (this.index === true) {
      this.index = "index.html";
    }
    if (typeof this.root === "object") {
      this.root.forEach(function(path) {
        return steps.push(serveStatic(path, {
          index: this.index
        }));
      });
    } else {
      steps.push(serveStatic(this.root, {
        index: this.index
      }));
    }
    if (this.fallback) {
      steps.push((function(_this) {
        return function(req, res) {
          var fallbackPath;
          fallbackPath = _this.fallback;
          if (typeof _this.fallback === "function") {
            fallbackPath = _this.fallback(req, res);
          }
          return require('fs').createReadStream(fallbackPath).pipe(res);
        };
      })(this));
    }
    return steps;
  };

  ConnectApp.prototype.log = function(text) {
    if (!this.silent) {
      return util.log(util.colors.green(text));
    }
  };

  ConnectApp.prototype.logWarning = function(text) {
    if (!this.silent) {
      return util.log(util.colors.yellow(text));
    }
  };

  ConnectApp.prototype.logDebug = function(text) {
    if (this.debug) {
      return util.log(util.colors.blue(text));
    }
  };

  ConnectApp.prototype.oldMethod = function(type) {
    var text;
    text = 'does not work in gulp-connect v 2.*. Please read "readme" https://github.com/AveVlad/gulp-connect';
    switch (type) {
      case "open":
        return this.logWarning("Option open " + text);
    }
  };

  return ConnectApp;

})();

module.exports = {
  start: function(options) {
    if (options == null) {
      options = {};
    }
    var app = new ConnectApp(options);
    apps.push(app);
    return app;
  },

  reload: function() {
    return es.map(function(file, callback) {
      apps.forEach((function(_this) {
        return function(app) {
          if (app.livereload && typeof app.lr === "object") {
            return app.lr.changed({
              body: {
                files: file.path
              }
            });
          }
        };
      })(this));
      return callback(null, file);
    });
  },

  stop: function() {
    apps.forEach(function(app) {
      return app.server.close();
    });
    return apps = [];
  }
};
