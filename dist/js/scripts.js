(function($) {

  function initDropdown() {
    $('.menu .drop').each(function() {
      var $drop = $(this);
      var $submenu = $drop.next();
      var $submenuInner = $submenu.children();
      $drop.click(function() {
        if ($drop.hasClass('expanded')) {
          $submenu.css('height', '');
          $drop.removeClass('expanded');
        } else {
          $submenu.height($submenuInner.outerHeight());
          $drop.addClass('expanded');
        }
      })
    });
  }

  function initSlider() {
    $(".slider-article").slick({
      prevArrow: "<button type='button' class='slick-prev'></button>",
      nextArrow: "<button type='button' class='slick-next'></button>",
      infinite: false,
    });
  }

  function initTripleSlider() {
    $(".slider-triple").each(function() {
      var $sliderTriple = $(this);
      var $inner = $sliderTriple.find(".slider-triple__inner");
      var $controls = $sliderTriple.find(".slider-triple__controls");
      var show2Max = $sliderTriple.hasClass('slider-triple--2-max');

      $inner.slick({
        appendArrows: $controls,
        appendDots: $controls,
        arrows: true,
        dots: true,
        prevArrow: "<button type='button' class='slick-prev'></button>",
        nextArrow: "<button type='button' class='slick-next'></button>",
        slidesToShow: show2Max ? 2 : 3,
        slidesToScroll: show2Max ? 2 : 3,
        infinite: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    });
  }

  function initPopup() {
    $(document).magnificPopup({
      type:'image',
      delegate: '.image-link',
      removalDelay: 300,
      tClose: 'Закрыть (Esc)',
      tLoading: 'Загрузка...',
      gallery:{
        enabled: true,
        tPrev: 'Назад',
        tNext: 'Вперед',
        tCounter: '%curr% из %total%'
      }
    });
  }

  function initSearch() {
    $('.search').each(function() {
      var $search = $(this);
      var $input = $search.find('input');
      var $icon = $search.find('.search__icon');

      if ($input.val()) {
        $icon.addClass('hidden');
      }

      $input.focus(function() {
        $icon.addClass('hidden');
      });

      $input.blur(function() {
        if ($input.val()) return;
        $icon.removeClass('hidden');
      });
    });

  }

  window.showContactForm = function() {
    var $contactForm = $('#contact-form');
    if (!$contactForm.length) return;
    $.magnificPopup.open({
      mainClass: 'content-popup',
      fixedContentPos: true,
      items: {
        src: $contactForm,
      },
      type: 'inline'
    });
  };

  $(function() {
    initDropdown();
    initSlider();
    initTripleSlider();
    initPopup();
    initSearch();
  });

})(jQuery);
