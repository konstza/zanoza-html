var autoprefixer = require('autoprefixer'),
    inlineSvg = require('postcss-inline-svg');

module.exports = {
  plugins: [
    autoprefixer({
      flexbox: 'no-2009'
    }),
    inlineSvg
  ]
}