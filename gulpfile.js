const gulp = require('gulp');
const watch = require('gulp-watch');

const css = require('./build/css');
const html = require ('./build/html');
const server = require('./build/server');
const svg = require('./build/svg');

gulp.task('server', () => {
  server.start({
    root: ['dist'],
    // root: ['dist', './../../app/assets-db/'],
    port: 9999,
    livereload: true
  })
})


gulp.task('css', () => {
  return css({ src: './src/scss/*.scss', sourcemap: true, minify: false, dest: './dist/css' })
});


gulp.task('html', function () {
  return html({ src: './src/pages/*.njk', dest: './dist', minify: false })
});


gulp.task('reload-css', ['css'], () => {
  return gulp.src('./dist/css/*.css').pipe(server.reload());
});


gulp.task('reload-html', ['html'], () => {
  return gulp.src('./dist/*.html').pipe(server.reload());
});


gulp.task('watch', ['css', 'html'], () => {
  gulp.watch(['./src/scss/**/*.scss'], ['reload-css']);
  gulp.watch(['./src/pages/**/*.njk', './src/pages/**/*.svg'], ['reload-html']);
})


gulp.task('default', [ 'watch', 'server' ]);
